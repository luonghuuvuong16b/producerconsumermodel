package com.projectz.service

import com.projectz.domain.{Channel, NumberGenerator, ProcessingResult, SumCalculator}

class GenAndCalculateService(channelLength:Int,numberOfGenerator:Int,numberOfCalculator:Int) {
  def isCompleted(producerSeq: Seq[NumberGenerator]): Boolean = {
    var result = true
    producerSeq.foreach(item => {
      if (!item.completed) result = false
    })
    result
  }

  def processing(): ProcessingResult = {
    val channel = new Channel(channelLength)
    val generatorList = for (_ <- 1 to numberOfGenerator) yield {
      val generator = new NumberGenerator(channel)
      val pThread = new Thread(generator)
      pThread.start()
      generator
    }
    val calculatorList: Seq[SumCalculator] = for (_ <- 1 to numberOfCalculator) yield {
      val calculator = new SumCalculator(channel)
      val cThread = new Thread(calculator)
      cThread.start()
      calculator
    }

    while (!isCompleted(generatorList)) {
      Thread.sleep(50)
    }
    ProcessingResult(producers = generatorList, consumers = calculatorList)
  }
}
