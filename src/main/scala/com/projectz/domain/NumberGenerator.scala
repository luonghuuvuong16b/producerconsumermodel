package com.projectz.domain

import scala.math.random
class NumberGenerator(channel:Channel) extends Runnable{
  var sum: (Long, Long) = (0,0)
  var count :Int= 0
  var completed=false
  override def run(): Unit = {
   while (count<10000){
      val num1:Int= (random()*1000).toInt
      val num2:Int= (random()*1000).toInt
      while (!channel.isAbleAddNewItem()) {
      }
     val t=  channel.add((num1,num2))
      if(t){
        count = count + 1
        sum = (sum._1 + num1, sum._2 + num2)
      }
    }
    println(s"Producer::run::count::${count}")
    println(s"Producer::run::Sum::${sum}")
    completed=true
  }


}
