package com.projectz.domain

case class ProcessingResult(producers: Seq[NumberGenerator],consumers: Seq[SumCalculator])