package com.projectz.domain

import scala.util.{Failure, Success, Try}

class  Channel(capacity:Int) {

 private var headPointer:Int =capacity/2
 private var tailPointer :Int=capacity/2
 private var amount = 0


 private var processQueue: Array[(Int, Int)] =Array.fill(capacity){(0,0)}
  def show(): Unit = this.synchronized{
    println(processQueue.mkString("Array(", ", ", ")"))
  }
  def  get(): Try[(Int,Int)]= this.synchronized{
       if(amount>0){
         amount=amount-1
         val item =processQueue(tailPointer)
         processQueue(tailPointer)=(0,0)
         if(tailPointer==0){
           tailPointer=capacity-1
         }else{
           tailPointer=tailPointer-1
         }
         Success(item)
       } else {
        Failure(new Exception("Is Empty"))
       }
  }
  def isAbleAddNewItem():Boolean=this.synchronized{
        amount<capacity
  }
  def add(item:(Int,Int)):Boolean= this.synchronized{
    if(amount<capacity){
      amount=amount+1
      processQueue(headPointer)=item
      if(headPointer==0) headPointer=capacity-1 else headPointer=headPointer-1
      true
    } else {
      false
    }
  }

}
