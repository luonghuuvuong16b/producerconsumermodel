package com.projectz.domain

class SumCalculator ( channel:Channel)extends Runnable{
  var sum:(Long,Long) = (0,0)
  var count=0
  override def run(): Unit = {
    while (true) {
      val item=channel.get()
      if(item.isSuccess){
        count = count + 1
        sum = (sum._1 + item.get._1, sum._2 + item.get._2)
      }
    }
  }
}
