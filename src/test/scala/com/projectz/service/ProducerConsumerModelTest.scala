package com.projectz.service

import com.google.inject.Guice
import com.projectz.domain.ProcessingResult
import com.projectz.module.MainModule
import com.twitter.inject.{Injector, IntegrationTest}

class ProducerConsumerModelTest  extends  IntegrationTest{
  override protected def injector: Injector =  Injector(Guice.createInjector(Seq(MainModule):_*))
  val modelTest=new GenAndCalculateService(channelLength = 100, numberOfGenerator = 20, numberOfCalculator = 40)
  test("Test Demo"){
    val result: ProcessingResult = modelTest.processing()
    var pSum:(Long,Long)= (0,0)
    var cSum:(Long,Long)= (0,0)
    var numberItemOfProducer=0
    var numberItemOfConsumer=0
    result.consumers.foreach(
      item=>{
        cSum=(cSum._1+item.sum._1,cSum._2+item.sum._2)
        numberItemOfConsumer=numberItemOfConsumer+item.count
      }
    )
    result.producers.foreach(
      item => {
        pSum = (pSum._1 + item.sum._1,pSum._2+item.sum._2)
        numberItemOfProducer=numberItemOfProducer+item.count
      }
    )
    assert(pSum==cSum && numberItemOfProducer==numberItemOfConsumer)
  }
}
